extends Area2D

var player


func _ready():
	set_process(false)


func _on_DoorTrigger_body_entered(body):
	if body.name == "Player":
		set_process(true)
		player = body


func _on_DoorTrigger_body_exited(body):
	if body.name == "Player":
		set_process(false)
		player = body


func _on_OpenTrigger_body_entered(body):
	if body.name == "Player":
		set_process(true)
		player = body


func _on_OpenTrigger_body_exited(body):
	if body.name == "Player":
		set_process(false)
		player = body


func _process(delta):
	DB.saveTrigger($CollisionShape2D.get_parent().name)
	var dir = player.get_parent().filename.split("/")[-2]
	DB.savePosition(dir)
	var position_player = DB.loadDB().get(dir).get("positionAnimatedA")
	var name_scene = DB.loadValue().get("positionB")

	if player:
		if player.get_node("AnimatedSprite").animation == position_player:
			SceneManager.goto_scene(name_scene)


func _on_OpenTrigger2_body_entered(body):
	if body.name == "Player":
		set_process(true)
		player = body


func _on_OpenTrigger2_body_exited(body):
	if body.name == "Player":
		set_process(false)
		player = body
