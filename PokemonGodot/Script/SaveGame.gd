extends Node

var dir = self.get_script().get_path().get_base_dir()
var FILE_NAME = dir + "/game-data.json"

var changescene = 0
var animationBot = 0

func _ready():
	pass

func saveStep(value):
	var data = loadValue()
	data["step"] = value
	var f = File.new()
	f.open(FILE_NAME, File.WRITE)
	f.store_string(JSON.print(data, "  ", true))
	f.close()


func savePosition(value):
	var data = loadValue()
	data["positionA"] = "res://Scenes/" + value + "/ScenaGame.tscn"
	data["positionB"] = (
		"res://Scenes/"
		+ DB.loadDB().get(value).get("name_sceneA")
		+ "/ScenaGame.tscn"
	)
	var f = File.new()
	f.open(FILE_NAME, File.WRITE)
	f.store_string(JSON.print(data, "  ", true))
	f.close()


func savePositionPlayer(posX, posY, nameCity):
	var data = loadValue()
	data["positionPlayerY"] = posY
	data["positionPlayerX"] = posX
	data["positionPlayerNameCity"] = nameCity
	var f = File.new()
	f.open(FILE_NAME, File.WRITE)
	f.store_string(JSON.print(data, "  ", true))
	f.close()


func saveTrigger(value):
	var data = loadValue()
	data["trigger"] = value
	var f = File.new()
	f.open(FILE_NAME, File.WRITE)
	f.store_string(JSON.print(data, "  ", true))
	f.close()


func savePokeball(value):
	var data = loadValue()
	data["pokeball"] = value
	var f = File.new()
	f.open(FILE_NAME, File.WRITE)
	f.store_string(JSON.print(data, "  ", true))
	f.close()


func loadDB():
	var file = File.new()
	file.open(dir + "/database.json", File.READ)
	var data = JSON.parse(file.get_as_text())
	file.close()
	return data.result


func loadValue():
	var file = File.new()
	if file.file_exists(FILE_NAME):
		file.open(FILE_NAME, File.READ)
		var data = JSON.parse(file.get_as_text())
		file.close()
		return data.result
	else:
		printerr("No saved data!")
		var f = File.new()
		f.open(FILE_NAME, File.WRITE)
		var json_text = {"step": 0, "pokeball": 0}
		f.store_string(JSON.print(json_text, "  ", true))
		f.close()
