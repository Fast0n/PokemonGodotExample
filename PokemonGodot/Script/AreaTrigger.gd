extends Area2D

var player_just_spawned = true
var player


func _on_ExitTrigger_body_entered(body):
	if body.name == "Player":
		set_process(true)
		player = body


func _on_ExitTrigger_body_exited(body):
	if body.name == "Player":
		set_process(false)
		player = body


func _on_DoorTrigger_body_entered(body):
	if body.name == "Player":
		set_process(true)
		player = body


func _on_DoorTrigger_body_exited(body):
	if body.name == "Player":
		set_process(false)
		player = body


func _process(delta):
	var dir = ""
	if player != null:
		dir = $CollisionShape2D.get_parent().get_parent().get_parent().filename.split("/")[-2]
		var position_player = DB.loadDB().get(dir).get("positionAnimatedB")
		var trigger = DB.loadValue().get("trigger")
		if trigger == "ExitTrigger":
			trigger = "FloorHome/" + DB.loadValue().get("trigger")
		var name_scene = DB.loadValue().get("positionA")

		if player and player.get_node("AnimatedSprite").animation == position_player:
			var spawn_data = {
				"spawn_on_node": trigger,
			}
			# change scene
			SceneManager.goto_scene(name_scene, spawn_data)
