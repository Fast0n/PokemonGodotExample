extends Node

var current_scene: Node = null


func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)


func goto_scene(path: String, params = null):
	call_deferred("_deferred_goto_scene", path, params)


func _deferred_goto_scene(path: String, params = null):
	current_scene.free()
	var s = ResourceLoader.load(path)
	current_scene = s.instance()
	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene(current_scene)
	if params:
		current_scene.init(params)


func add_node_to_scene(node):
	current_scene.add_child(node)


func load_node(node_scene_path):
	var new_node_scene = load(node_scene_path)
	var new_node = new_node_scene.instance()
	return new_node


func reparent_node_to(node: Node, new_parent: Node):
	var old_parent = node.get_parent()
	old_parent.remove_child(node)
	# TODO: free memory?
	new_parent.add_child(node)
