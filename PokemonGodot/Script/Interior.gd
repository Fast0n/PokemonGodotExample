extends Node

onready var player: KinematicBody2D = $Player
var position_player


func init(params):
	var dir = $FloorHome.get_parent().filename.split("/")[-2]
	for i in DB.loadDB():
		if DB.loadDB().get(i).get("name_sceneA") == dir:
			DB.savePosition(i)
			break
	position_player = DB.loadDB().get(dir).get(dir)
	var spawning_point = ""
	if params:
		if DB.changescene == 1:
			var posx = DB.loadValue().get("positionPlayerX")
			var posy = DB.loadValue().get("positionPlayerY")
			player.position = Vector2(posx, posy)
			DB.changescene = 2

		else:
			DB.saveTrigger("OpenTrigger")
			spawning_point = get_node(params.spawn_on_node).global_position
			$Player.global_position = spawning_point
			var sprite = $Player.get_node("AnimatedSprite")
			if position_player == "stand_right":
				sprite.play("stand_left")
				sprite.set_flip_h(true)
			else:
				sprite.play(position_player)


func _ready():
	var dir = $FloorHome.get_parent().filename.split("/")[-2]

	if DB.changescene == 0 and DB.loadValue().get("positionPlayerNameCity") != null:
		if DB.loadValue().get("positionPlayerNameCity") != dir:
			var name_scene = (
				"res://Scenes/"
				+ DB.loadValue().get("positionPlayerNameCity")
				+ "/ScenaGame.tscn"
			)
			SceneManager.goto_scene(name_scene, "spawn_data")
			DB.changescene = 1
	else:
		position_player = DB.loadDB().get(dir).get("stand_B")
		player.position = $FloorHome/ExitTrigger.position * 3
		player.get_node("AnimatedSprite").play(position_player)
