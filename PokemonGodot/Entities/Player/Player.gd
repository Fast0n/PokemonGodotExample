extends KinematicBody2D

var speed_walk = 250

signal step_thrown
signal dialogue_started
signal menu
signal name_city
var boolean = true


func _ready():
	pass


func _process(delta):
	# Inizializzo il vettore "velocity"
	var velocity = Vector2()

	# Aggiorno il vettore velocity in base all'input del giocatore
	if Input.is_key_pressed(KEY_UP) and !Input.is_key_pressed(KEY_LEFT) and !Input.is_key_pressed(KEY_RIGHT):
		if boolean == true:
			velocity.y = -1
			DB.saveStep(DB.loadValue().get("step") + 1)
			$RayCast2D.cast_to = Vector2(0, -50)
	if Input.is_key_pressed(KEY_DOWN) and !Input.is_key_pressed(KEY_LEFT) and !Input.is_key_pressed(KEY_RIGHT):
		if boolean == true:
			velocity.y = 1
			DB.saveStep(DB.loadValue().get("step") + 1)
			$RayCast2D.cast_to = Vector2(0, 50)
	if Input.is_key_pressed(KEY_LEFT):
		if boolean == true:
			velocity.x = -1
			DB.saveStep(DB.loadValue().get("step") + 1)
			$RayCast2D.cast_to = Vector2(-50, 0)
	if Input.is_key_pressed(KEY_RIGHT):
		if boolean == true:
			velocity.x = 1
			DB.saveStep(DB.loadValue().get("step") + 1)
			$RayCast2D.cast_to = Vector2(50, 0)

	if Input.is_key_pressed(KEY_S):
		emit_signal("name_city", get_node("/root/Node"), $CollisionShape2D.get_parent().position)
		emit_signal("step_thrown", DB.loadValue().get("step") / 5)
		speed_walk = 500
	else:
		speed_walk = 250
		emit_signal("name_city", get_node("/root/Node"), $CollisionShape2D.get_parent().position)
		emit_signal("step_thrown", DB.loadValue().get("step") / 5)

	var movement = speed_walk * velocity.normalized() * delta

	self.move_and_collide(movement)
	self.update_animated_sprite(velocity)

	# se il raycast ha una collisione con un oggetto
	if $RayCast2D.is_colliding():
		# salva il collider in una variabile
		var collider = $RayCast2D.get_collider()
		# se il collider è una pokeball ed abbiamo premuto il tasto spazio

		if collider != null and Input.is_key_pressed(KEY_A) and "Pokeball" in collider.name:
			# "raccogli" la pokeball
			collider.queue_free()  # elimina il nodo dalla scena
			DB.savePokeball(DB.loadValue().get("pokeball") + 1)

		if (
			collider != null
			and Input.is_action_just_released("ui_accept")
			and "Character" in collider.get_groups()
		):
			
			DB.animationBot = 1
			emit_signal("dialogue_started", str(collider.name) + ": Hi!")
			$AnimatedSprite.stop()
			set_process(false)


func update_animated_sprite(velocity):
	if velocity.x == -1:
		$AnimatedSprite.flip_h = false
		$AnimatedSprite.play('walk_left')
	elif velocity.x == 1:
		# specchia la sprite per creare l'animazione di camminata verso destra
		$AnimatedSprite.flip_h = true
		$AnimatedSprite.play('walk_left')
	elif velocity.y == -1:
		$AnimatedSprite.play('walk_up')
	elif velocity.y == 1:
		$AnimatedSprite.play('walk_down')

	if velocity == Vector2():
		# se il giocatore si stava spostando verso sinistra
		if $AnimatedSprite.animation == 'walk_left':
			$AnimatedSprite.play('stand_left')
		elif $AnimatedSprite.animation == 'walk_up':
			$AnimatedSprite.play('stand_up')
		elif $AnimatedSprite.animation == 'walk_down':
			$AnimatedSprite.play('stand_down')

func _on_UI_dialogue_finisched():
	var collider = $RayCast2D.get_collider()
	DB.animationBot = 0
	set_process(true)


func _on_UI_show_menu(index):
	boolean = index
	$Bag.start()
