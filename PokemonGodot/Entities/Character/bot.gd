extends KinematicBody2D

var movement = Vector2()
var speed = 200

func _ready():
	randomize()
	pass


func _process(delta):

	if DB.animationBot == 1:
		$AnimatedSprite.stop()
	else:	
		if $Timer.time_left == 0:
			$Timer.start()
			if randf() < 0.5:
				movement = Vector2()
			else:
				movement = get_random_direction()
		move_and_collide(movement * speed * delta)
		update_animated_sprite(movement)



func get_random_direction():
	var random_direction = Vector2()
	var random_float = randf()

	if random_float < 0.25:
		random_direction.x = -1
	elif random_float >= 0.25 and random_float < 0.5:
		random_direction.x = 1
	elif random_float >= 0.5 and random_float < 0.75:
		random_direction.y = 1
	elif random_float >= 0.75 and random_float < 1:
		random_direction.y = -1

	return random_direction


func update_animated_sprite(velocity):
	if velocity.x == -1:
		$AnimatedSprite.flip_h = false
		$AnimatedSprite.play('walk_left')
	elif velocity.x == 1:
		# specchia la sprite per creare l'animazione di camminata verso destra
		$AnimatedSprite.flip_h = true
		$AnimatedSprite.play('walk_left')
	elif velocity.y == -1:
		$AnimatedSprite.play('walk_up')
	elif velocity.y == 1:
		$AnimatedSprite.play('walk_down')

	if velocity == Vector2():
		# se il giocatore si stava spostando verso sinistra
		if $AnimatedSprite.animation == 'walk_left':
			$AnimatedSprite.play('stand_left')
		elif $AnimatedSprite.animation == 'walk_up':
			$AnimatedSprite.play('stand_up')
		elif $AnimatedSprite.animation == 'walk_down':
			$AnimatedSprite.play('stand_down')
