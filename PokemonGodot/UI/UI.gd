extends Node

signal dialogue_text
signal dialogue_finisched
signal show_menu
signal position_player


func tB_wait(s, obj):
	obj.show()
	var t = Timer.new()
	t.set_wait_time(s)
	add_child(t)
	t.start()
	yield(t, "timeout")
	obj.hide()
	yield(get_tree().create_timer(0), "timeout")
	set_process(false)


func _ready():
	tB_wait(1.5, $TextBox2)
	set_process(false)
	$TextBox.hide()


func _process(delta):
	if Input.is_action_just_released("ui_accept"):
		$TextBox.hide()
		set_process(false)
		emit_signal("dialogue_finisched")


func _on_username_pokeball_thrown(pokeball_count):
	$TextureRect/Pokeball.text = str(DB.loadValue().get("pokeball"))


func _on_username_step_thrown(step_count):
	$TextureRect/Step.text = str(int(step_count))
	$TextureRect/Pokeball.text = str(DB.loadValue().get("pokeball"))


func _on_username_dialogue_started(dialogue_text):
	$TextBox/Label.text = dialogue_text
	$TextBox.show()
	set_process(true)


func _on_username_name_city(text, position):
	var name_city = text.filename.split("/")[-2]
	$TextBox2/Label.text = name_city
	emit_signal("position_player", position, name_city)


func _on_username_menu(boolean):
	if boolean:
		$Menu.show()
	else:
		$Menu.hide()


func _on_NinePatchRect_menu(text):
	if text:
		$Menu.show()
		emit_signal("show_menu", false)
	else:
		$Menu.hide()
		emit_signal("show_menu", true)


func _on_Sprite_allert(text):
	tB_wait(1.5, $TextBox)
	$TextBox/Label.text = text
