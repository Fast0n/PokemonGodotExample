extends Sprite

var positions = []
var index = 0
var was_pressed = 0
var positionPlayerX = 0
var positionPlayerY = 0
var positionPlayerCity = 0
signal menu
signal allert


func _ready():
	for node in get_parent().get_children():
		if node is Label and node.visible:
			positions.append(node)


func set_selection(new_index):
	if 0 <= new_index and new_index < len(positions):
		index = new_index
		var selected_node = positions[index]
		position = Vector2(selected_node.rect_position.x, selected_node.rect_position.y)


func _process(delta):
	if $NinePatchRect.get_parent().get_parent().get_parent().visible:
		if Input.is_action_just_pressed("ui_up"):
			set_selection(index - 1)
		if Input.is_action_just_pressed("ui_down"):
			set_selection(index + 1)

		if ! Input.is_key_pressed(KEY_E) and was_pressed == 1:
			emit_signal("menu", false)
			was_pressed = 0
		elif Input.is_key_pressed(KEY_E):
			was_pressed = 1

		if Input.is_key_pressed(KEY_A) and was_pressed == 0:
			if index == 2:
				emit_signal("allert", "Salvataggio completato")
				DB.savePositionPlayer(positionPlayerX, positionPlayerY, positionPlayerCity)
				was_pressed = 0

			elif index == 3:
				emit_signal("menu", false)
			was_pressed = 0

	else:
		if ! Input.is_key_pressed(KEY_E) and was_pressed == 1:
			emit_signal("menu", true)
			was_pressed = 0
		elif Input.is_key_pressed(KEY_E):
			was_pressed = 1


func _on_UI_position_player(posPlayer, nameCity):
	positionPlayerX = posPlayer.x
	positionPlayerY = posPlayer.y
	positionPlayerCity = nameCity
